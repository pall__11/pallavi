git config --global user.name "Pallavi"
The GNU’s General Public License is the most popular open source license around. Richard Stallman created the GPL to protect the GNU software from becoming proprietary, and it is a specific implementation of his "copyleft" concept.

GPL is a copyleft license. This means that any software that is written based on any GPL component must be released as open source. The result is that any software that uses any GPL open source component (regardless of its percentage in the entire code) is required to release its full source code and all of the rights to modify and distribute the entire code.

There has always been some confusion regarding what constitutes a ‘work based on’ another work, which in turn triggers the GPL reciprocity obligation. The FSF tried to add more clarity to GPLv3 as to when the reciprocity obligation is triggered. The FSF even wrote a new GPL license, the Affero license, to address a specific confusion referred to as the “ASP loophole”.

In addition, the FSF tried to increase the compatibility of the GPLv3 with other licenses. To combine two codes into a larger work, both the programs must permit it. If such rights are granted by both the programs' licenses, they are compatible. By making the GPLv3 more compatible, the FSF expanded development options.

The third difference between the two versions is that the GPLv3 was written in an attempt to increase usage worldwide. The language used in GPLv3 to describe the license rights was modified to ensure that international laws will interpret it as the FSF intended, unlike the language used in GPLv2, which is considered very US-centric. GPLv3 also allows developers to add local disclaimers, which also helps increase its usage outside the US.